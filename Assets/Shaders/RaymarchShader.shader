﻿
//https://www.shadertoy.com/view/XtjSDK

Shader "Raymarch/RaymarchShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "DistanceFunctions.cginc"

			#include "FBM.cginc"

			#define MAX_NUMBER_OF_LIGHTS 4
			#define MAX_NUMBER_OF_MATERIALS 10
			#define AA 2

			sampler2D _MainTex;
			// Setup
			uniform sampler2D _CameraDepthTexture;
			uniform float4x4 _CamFrustum, _CamToWorld;
			uniform int _MaxIterations;
			uniform float _StepScale;
			uniform float _Accuracy;
			uniform float _MaxDistance;
			// Materials
			uniform int _NumberOfMaterials;
			uniform fixed4 _MaterialAlbedo[MAX_NUMBER_OF_MATERIALS];
			uniform float _MaterialSpecular[MAX_NUMBER_OF_MATERIALS];
			uniform float _MaterialReflectiveness[MAX_NUMBER_OF_MATERIALS];
			uniform float _ColorIntensity;
			// Light
			uniform int _NumberOfLights;
			uniform float3 _LightDir[MAX_NUMBER_OF_LIGHTS];
			uniform float3 _LightColor[MAX_NUMBER_OF_LIGHTS];
			uniform float _LightIntensity[MAX_NUMBER_OF_LIGHTS];
			// Shadow
			uniform float2 _ShadowDistance;
			uniform float _ShadowIntensity;
			uniform float _ShadowPenumbra;
			// Ambient Occlusion
			uniform float _AoStepSize;
			uniform int _AoIterations;
			uniform float _AoIntensity;
			// Reflections
			uniform int _ReflectionCount;
			uniform float _ReflectionIntensity;
			uniform float _EnvRefIntensity;
			uniform samplerCUBE _ReflectionCube;
			// SDF
			uniform float4x4 _ObjectTransformMatrix[MAX_NUMBER_OF_MATERIALS];
			uniform float4 _Sphere;
			uniform float _SphereSmooth;
			uniform float _DegreeRotate;

			struct _Material
			{
				fixed3 albedo;
				float specular;
			};


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 pos : SV_POSITION;
				float3 ray : TEXCOORD1;
			};

			v2f vert(appdata v)
			{
				v2f o;
				half index = v.vertex.z;
				v.vertex.z = 0;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv.xy;

				o.ray = _CamFrustum[(int)index].xyz;
				o.ray /= abs(o.ray.z);
				o.ray = mul(_CamToWorld, o.ray);

				return o;
			}

			float4 distanceField(float3 p)
			{
			
				float3 boxP = (mul(_ObjectTransformMatrix[0], float4(p, 1)).xyz);
				float noise = FBMT((p*0.4), 3) * 2;
				boxP.y += noise;
				float4 box = float4(_MaterialAlbedo[0].rgb, sdBox(boxP, float3(1,1,1)));

			/*
				float4 ground = float4(_MaterialAlbedo[0].rgb, sdBox(groundP, float3(1, 0.2, 1)));

				float3 ShapeP = (mul(_ObjectTransformMatrix[1], float4(p, 1)).xyz);

				float d = 1e10;
				float3 q = opTwistY(ShapeP, sin(_Time*5) * 2) ;
				q = -opTwistX(q, sin(_Time * 5) * -4);
				float4 w = opElongate(q.xyz, float3(0.2, 0.2, 0.3));
				d = min(d, w.w + sdTorus(w.xyz, float2(0.4, 0.05)));


		
				float4 s = float4(_MaterialAlbedo[1].rgb, d);
				return opUS(s, ground, _SphereSmooth);
				*/
				return  box;
			}

			float3 getNormal(float3 p)
			{
				const float2 offset = float2(0.001, 0.0);

				float3 n = float3(
					distanceField(p + offset.xyy).w - distanceField(p - offset.xyy).w,
					distanceField(p + offset.yxy).w - distanceField(p - offset.yxy).w,
					distanceField(p + offset.yyx).w - distanceField(p - offset.yyx).w);

				return normalize(n);
			}

			float hardShadow(float3 ro, float3 rd, float mint, float maxt)
			{
				for (float t = mint; t < maxt;)
				{
					float h = distanceField(ro + rd * t).w;
					if (h < 0.001)
					{
						return 0.0;
					}
					t += h;
				}
				return 1.0;
			}

			float softShadow(float3 ro, float3 rd, float mint, float maxt, float k)
			{
				float result = 1.0;
				for (float t = mint; t < maxt;)
				{
					float h = distanceField(ro + rd * (t*_StepScale)).w;
					if (h < 0.001)
					{
						return 0.0;
					}
					result = min(result, k*h / t);
					t += h;
				}
				return result;
			}

			float ambientOcclusion(float3 p, float3 n) {

				float step = _AoStepSize;
				float ao = 0.0;
				float dist;

				for (int i = 1; i <= _AoIterations; i++) {
					dist = step * i;
					ao += max(0.0, (dist - distanceField(p + n * dist).w) / dist);
				}
				return (1.0 - ao * _AoIntensity);

			}

			float3 shading(float3 p, float3 n, fixed3 c)
			{

				// Diffuse color
				float3 color = c.rgb * _ColorIntensity;
				float3 light = float3(0, 0, 0);
				float shadow = 0;
				for (int i = 0; i < min(_NumberOfLights, MAX_NUMBER_OF_LIGHTS); i++) {
					float specularStrength = 0.5;
					
					float diff = max(dot(n, -_LightDir[i]), 0.0);
					float3 diffuse = diff * _LightColor[i];

					float3 reflectDir = reflect(-_LightDir[i], n);
					float spec = pow(max(dot(_LightDir[i], reflectDir), 0.0), 32);
					
					// Directional Light
					light += ((diffuse) + (specularStrength * spec ))* _LightColor[i] * _LightIntensity[i];//(_LightColor[i] * dot(-_LightDir[i], n) * 0.5 + 0.5) * _LightIntensity[0];
					// Shadows
					float s = softShadow(p, -_LightDir[i], _ShadowDistance.x, _ShadowDistance.y, _ShadowPenumbra) * 0.5 + 0.5;
					shadow += max(0.0, pow(s, _ShadowIntensity));
				}
				light /= min(_NumberOfLights, MAX_NUMBER_OF_LIGHTS); //????
				
			
				//Ambient Occlusion
				float ao = ambientOcclusion(p, n);

				float3 result = color * light * shadow * ao;
				return result;
			}

			bool raymarching(float3 ro, float3 rd, float depth, float maxDistance, int maxIteration, inout float3 p, inout fixed3 dColor, inout _Material material)
			{
				bool hit;
		
				float t = 0; // distance travelled along the ray direction

				for (int i = 0; i < maxIteration; i++)
				{
					if (t > maxDistance /*|| t > depth*/)
					{
						//Envoiremnt
						hit = false;
						break;
					}

					p = ro + rd * (t*_StepScale);

					float4 d = distanceField(p); //check for hit in distance field

					if (d.w < _Accuracy)  // we have a hit
					{
						dColor = d.rgb;
						material.albedo = d.rgb;
						hit = true;
						break;
					}
					t += d.w;
				}

				return hit;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed3 col = tex2D(_MainTex, i.uv);

				float depth = LinearEyeDepth(tex2D(_CameraDepthTexture, i.uv).r);
				depth *= length(i.ray);
		
				float3 tot = float3(0,0,0);
				float3 p = i.ray.xyz; 
				fixed4 result;
				float3 hitPosition;
				fixed3 dColor;

				_Material material;
				material.albedo = fixed3(0, 0, 0);
				material.specular = 0;


				float3 rayOrigin = _WorldSpaceCameraPos;
				float3 rayDirection = normalize(p);

				
				for (int m = 0; m < AA; m++) {
					for (int n = 0; n < AA; n++)
					{
						// pixel coordinates
						float2 o = float2(float(m), float(n)) / float(AA) - 0.5;
						p += float3(o / _ScreenParams.xy, 0);

						bool hit = raymarching(rayOrigin, rayDirection, depth, _MaxDistance, _MaxIterations, hitPosition, dColor, material);
						if (hit)
						{
							//shading 
							float3 n = getNormal(hitPosition);
							float3 s = shading(hitPosition, n, material.albedo);
							result = fixed4(s, 1);

							//Reflection
							if (_ReflectionCount > 0)
							{
								result += fixed4(texCUBE(_ReflectionCube, n).rgb * _EnvRefIntensity * _ReflectionIntensity, 0);
								rayDirection = normalize(reflect(rayDirection, n));
								rayOrigin = hitPosition + (rayDirection * 0.01);
								hit = raymarching(rayOrigin, rayDirection, _MaxDistance, _MaxDistance * 0.5, _MaxIterations / 2, hitPosition, dColor, material);
								if (hit)
								{
									float3 n = getNormal(hitPosition);
									float3 s = shading(hitPosition, n, material.albedo);
									result += fixed4(s * _ReflectionIntensity, 0);
									if (_ReflectionCount > 1)
									{
										rayDirection = normalize(reflect(rayDirection, n));
										rayOrigin = hitPosition + (rayDirection * 0.01);
										hit = raymarching(rayOrigin, rayDirection, _MaxDistance, _MaxDistance * 0.25, _MaxIterations / 4, hitPosition, dColor, material);
										if (hit)
										{
											float3 n = getNormal(hitPosition);
											float3 s = shading(hitPosition, n, material.albedo);
											result += fixed4(s * _ReflectionIntensity * 0.5, 0);
										}
									}
								}
							}
						}
						else
						{
							result = fixed4(0, 0, 0, 0);
						}
						tot += col * (1.0- result.w) + result.xyz * (result.w);
					}
				}
				tot /= float(AA*AA);
				return fixed4(tot,1.0);
			
			}
			ENDCG
		}
	}
}
