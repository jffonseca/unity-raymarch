﻿// Sphere
// s: radius
float sdSphere(float3 p, float s)
{
	return length(p) - s;
}

// Box
// b: size of box in x/y/z
float sdBox(float3 p, float3 b)
{
	float3 d = abs(p) - b;
	return min(max(d.x, max(d.y, d.z)), 0.0) +
		length(max(d, 0.0));
}

float sdRoundBox(in float3 p, in float3 b, in float r)
{
	float3 q = abs(p) - b;
	return length(max(q, 0.0)) + min(max(q.x, max(q.y, q.z)), 0.0) - r;
}

float sdPlane(float3 p, float4 n)
{
	return dot(p, n.xyz) + n.w;
}

float sdTorus(float3 p, float2 t)
{
	float2 q = float2(length(p.xz) - t.x, p.y);
	return length(q) - t.y;
}

float sdCappedTorus( float3 p, float2 sc,  float ra,  float rb)
{
	p.x = abs(p.x);
	float k = (sc.y*p.x > sc.x*p.y) ? dot(p.xy, sc) : length(p.xy);
	return sqrt(dot(p, p) + ra * ra - 2.0*ra*k) - rb;
}

// BOOLEAN OPERATORS //

// Union
float opU(float d1, float d2)
{
	return min(d1, d2);
}

float4 opU(float4 d1, float4 d2)
{
	return (d1.w < d2.w) ? d1 : d2;
}

// Subtraction
float opS(float d1, float d2)
{
	return max(-d1, d2);
}

// Intersection
float opI(float d1, float d2)
{
	return max(d1, d2);
}

// Mod Position Axis
float pMod1 (inout float p, float size)
{
	float halfsize = size * 0.5;
	float c = floor((p+halfsize)/size);
	p = fmod(p+halfsize,size)-halfsize;
	p = fmod(-p+halfsize,size)-halfsize;
	return c;
}


float4 opUS(float4 d1, float4 d2, float k)
{
	float h = clamp(0.5 + 0.5*(d2.w - d1.w) / k, 0.0, 1.0);
	float3 color = lerp(d2.rgb, d1.rgb, h);
	float dist = lerp(d2.w, d1.w, h) - k * h*(1.0 - h);
	return float4(color, dist);
}

float opSS(float d1, float d2, float k)
{
	float h = clamp(0.5 - 0.5*(d2 + d1) / k, 0.0, 1.0);
	return lerp(d2, -d1, h) + k * h*(1.0 - h);
}

float opIS(float d1, float d2, float k)
{
	float h = clamp(0.5 - 0.5*(d2 - d1) / k, 0.0, 1.0);
	return lerp(d2, d1, h) + k * h*(1.0 - h);
}

float3 rotateY(float3 v, float degree) {
	float rad = 0.017453925 * degree;
	float cosY = cos(rad);
	float sinY = sin(rad);
	return float3(cosY * v.x - sinY * v.z, v.y, sinY * v.x + cosY * v.z);
}

float4 opElongate(in float3 p, in float3 h)
{
	//return vec4( p-clamp(p,-h,h), 0.0 ); // faster, but produces zero in the interior elongated box

	float3 q = abs(p) - h;
	return float4(max(q, 0.0), min(max(q.x, max(q.y, q.z)), 0.0));
}

float3 opTwistY(float3 p, float k)
{
	float c = cos(k*p.y);
	float s = sin(k*p.y);
	float2x2  m = float2x2(c, -s, s, c);
	float3  q = float3(mul(p.xz,m), p.y); 
	return q;
}

float3 opTwistX(float3 p, float k)
{

	float c = cos(k*p.x);
	float s = sin(k*p.x);
	float2x2  m = float2x2(c, -s, s, c);
	float3  q = float3(mul(p.yz, m), p.x);
	return q;
}

float3 opCheapBend(float3 p, float k)
{
	float c = cos(k*p.x);
	float s = sin(k*p.x);
	float2x2  m = float2x2(c, -s, s, c);
	float3 q = float3(mul(m, p.xy), p.z);
	return q;
}
