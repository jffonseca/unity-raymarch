﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaymarchMaterial : MonoBehaviour
{
    public Color _Albedo;
    public float _Specular;
    private Transform transform;

    public Color getAlbedo()
    {
        return _Albedo;
    }

    public float getSpecular()
    {
        return _Specular;
    }

    public Matrix4x4 getTransform()
    {
        return gameObject.transform.worldToLocalMatrix;
    }
}
