﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class RaymarchCamera : SceneViewFilter
{
    [SerializeField]
    private Shader _shader;

    public Material _raymarchMaterial
    {
        get
        {
            if (!_raymarchMat && _shader)
            {
                _raymarchMat = new Material(_shader);
                _raymarchMat.hideFlags = HideFlags.HideAndDontSave;
            }
            return _raymarchMat;
        }
    }

    private Material _raymarchMat;

    public Camera _camera
    {
        get
        {
            if (!_cam)
            {
                _cam = GetComponent<Camera>();
            }
            return _cam;
        }
    }
    private Camera _cam;
    private Light[] _lights;
    private RaymarchMaterial[] _materials;

    [Header("Ray March")]
    public int _MaxIterations;
    [Range(0.01f,1.0f)]
    public float _StepScale;
    public float _MaxDistance;
    [Range(0.1f, 0.0001f)]
    public float _Accuracy;
    public Texture2D _NoiseTex;

    [Header("Shadow")]
    [Range(0, 4)]
    public float _ShadowIntensity;
    public Vector2 _ShadowDistance;
    [Range(1, 128)]
    public float _ShadowPenumbra;

    [Header("Ambient Occlusion")]
    [Range(1, 5)]
    public int _AoIterations;
    [Range(0.01f, 10.0f)]
    public float _AoStepSize;
    [Range(0, 1)]
    public float _AoIntensity;

    [Header("Reflection")]
    [Range(0, 2)]
    public int _ReflectionCount;
    [Range(0, 1)]
    public float _ReflectionIntensity;
    [Range(0, 1)]
    public float _EnvRefIntensity;
    public Cubemap _ReflectionCube;

    [Header("Materials")]
    public RaymarchMaterial[] _Materials;

    [Header("SDF")]
    public Vector4 _Sphere;
    public float _SphereSmooth;
    public float _DegreeRotate;

    [Header("Color")]
    public Color _GroundColor;
	public Gradient _SphereGradient;
    private Color[] _SphereColor = new Color[8];
    [Range(0, 4)]
    public float _ColorIntensity;
    

    public void Start()
    {
        
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
       
        if (!_raymarchMaterial)
        {
            Graphics.Blit(source, destination);
            return;
        }

        _SphereColor = new Color[8];
        for (int i = 0; i < 8; i++) {
            _SphereColor[i] = _SphereGradient.Evaluate((1f / 8f) * i);
        }

        _raymarchMaterial.SetMatrix("_CamFrustum", CamFrustum(_camera));
        _raymarchMaterial.SetMatrix("_CamToWorld", _camera.cameraToWorldMatrix);
        _raymarchMaterial.SetInt("_MaxIterations", _MaxIterations);
        _raymarchMaterial.SetFloat("_StepScale", _StepScale);
        _raymarchMaterial.SetFloat("_MaxDistance", _MaxDistance);
        _raymarchMaterial.SetFloat("_Accuracy", _Accuracy);
        _raymarchMaterial.SetTexture("_NoiseTex", _NoiseTex);

        _raymarchMaterial.SetVector("_Sphere", _Sphere);
        _raymarchMaterial.SetFloat("_SphereSmooth", _SphereSmooth);
        _raymarchMaterial.SetFloat("_DegreeRotate", _DegreeRotate);

        setShaderMaterials();
        setShaderLights();

        _raymarchMaterial.SetFloat("_ShadowIntensity", _ShadowIntensity);
        _raymarchMaterial.SetVector("_ShadowDistance", _ShadowDistance);
        _raymarchMaterial.SetFloat("_ShadowPenumbra", _ShadowPenumbra);

        _raymarchMaterial.SetInt("_AoIterations", _AoIterations);
        _raymarchMaterial.SetFloat("_AoStepSize", _AoStepSize);
        _raymarchMaterial.SetFloat("_AoIntensity", _AoIntensity);

        _raymarchMaterial.SetInt("_ReflectionCount", _ReflectionCount);
        _raymarchMaterial.SetFloat("_ReflectionIntensity", _ReflectionIntensity);
        _raymarchMaterial.SetFloat("_EnvRefIntensity", _EnvRefIntensity);
        _raymarchMaterial.SetTexture("_ReflectionCube", _ReflectionCube);

        _raymarchMaterial.SetColor("_GroundColor", _GroundColor);
        _raymarchMaterial.SetColorArray("_SphereColor", _SphereColor);
        _raymarchMaterial.SetFloat("_ColorIntensity", _ColorIntensity);

        RenderTexture.active = destination;
        _raymarchMaterial.SetTexture("_MainTex", source);
        GL.PushMatrix();
        GL.LoadOrtho();
        _raymarchMaterial.SetPass(0);
        GL.Begin(GL.QUADS);

        //BL
        GL.MultiTexCoord2(0, 0.0f, 0.0f);
        GL.Vertex3(0.0f, 0.0f, 3.0f);
        //BR
        GL.MultiTexCoord2(0, 1.0f, 0.0f);
        GL.Vertex3(1.0f, 0.0f, 2.0f);
        //TR
        GL.MultiTexCoord2(0, 1.0f, 1.0f);
        GL.Vertex3(1.0f, 1.0f, 1.0f);
        //TL
        GL.MultiTexCoord2(0, 0.0f, 1.0f);
        GL.Vertex3(0.0f, 1.0f, 0.0f);

        GL.End();
        GL.PopMatrix();
    }

    private Matrix4x4 CamFrustum(Camera cam)
    {
        Matrix4x4 frustum = Matrix4x4.identity;
        float fov = Mathf.Tan((cam.fieldOfView * 0.5f) * Mathf.Deg2Rad);

        Vector3 goUp = Vector3.up * fov;
        Vector3 goRight = Vector3.right * fov * cam.aspect;

        Vector3 TL = (-Vector3.forward - goRight + goUp);
        Vector3 TR = (-Vector3.forward + goRight + goUp);
        Vector3 BR = (-Vector3.forward + goRight - goUp);
        Vector3 BL = (-Vector3.forward - goRight - goUp);

        frustum.SetRow(0, TL);
        frustum.SetRow(1, TR);
        frustum.SetRow(2, BR);
        frustum.SetRow(3, BL);

        return frustum;
    }

    private void setShaderMaterials()
    {

        List<Color> albedo = new List<Color>();
        List<float> specular = new List<float>();
        List<Matrix4x4> matrix = new List<Matrix4x4>();
        int numberOfMaterials = 0;
        
        for (int i = 0; i < _Materials.Length; i++)
        {
            albedo.Add(_Materials[i].getAlbedo());
            specular.Add(_Materials[i].getSpecular());
            matrix.Add(_Materials[i].getTransform());
            numberOfMaterials++;
        }
        _raymarchMaterial.SetColorArray("_MaterialAlbedo", albedo);
        _raymarchMaterial.SetFloatArray("_MaterialSpecular", specular);
        _raymarchMaterial.SetMatrixArray("_ObjectTransformMatrix", matrix);
        _raymarchMaterial.SetInt("_NumberOfMaterials", numberOfMaterials);
    }

        private void setShaderLights()
    {
        List<Vector4> ld = new List<Vector4>();
        List<Color> lc = new List<Color>();
        List<float> li = new List<float>();
        int numberOfLights = 0;
        _lights = FindObjectsOfType(typeof(Light)) as Light[];
        foreach (Light light in _lights)
        {
            ld.Add(light.transform.forward);
            lc.Add(light.color);
            li.Add(light.intensity);
            numberOfLights++;
        }
        _raymarchMaterial.SetVectorArray("_LightDir", ld );
        _raymarchMaterial.SetColorArray("_LightColor", lc);
        _raymarchMaterial.SetFloatArray("_LightIntensity", li);
        _raymarchMaterial.SetInt("_NumberOfLights", numberOfLights);
    }
}
